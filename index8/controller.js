angular.module("exampleApp").controller("defaultController", function($scope, $http) {
		
	$scope.sendRequest = function() {	
		$http.get("data.json").success(function(response) {
			$scope.items = response;
		});
	}
});