//assign handlers to event for each elements
angular.module("exampleApp").directive("triButton", function() {
	return {
		//isolated directive scope
		scope: { counter: "=counter"},
		link: function(scope, element, attrs) {
			element.on("click", function(event) {
				console.log("Button click: ", + event.target.innerText);
				scope.$apply(function() {
					scope.counter++;
				});
			});
		}
	}
})