angular.module("exampleApp").controller("defaultController", function($scope) {
			
	$scope.buttons = {
		names: ["Butt #1", "Butt #2", "Butt #3"],
		totalClicks: 0 
	};

	// watch changes in buttons.totalClicks
	$scope.$watch('buttons.totalClicks', function(newVal) {
		console.log("Total click count" + newVal);
	});
});